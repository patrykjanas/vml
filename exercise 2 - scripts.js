(function(){

const slider = document.querySelector('.container');
let isMouseDown = false,
    startPosition,
    scroll;

slider.addEventListener('touchstart', function(e) {
	isMouseDown = true;
	startPosition = e.touches[0].clientX;
	scroll = slider.scrollLeft;
	console.log();
}, false);

slider.addEventListener('touchend', function(e) {
	isMouseDown = false;
}, false);

slider.addEventListener("touchmove", function(e) {
	if(!isMouseDown) return;
	e.preventDefault();
	const x = e.touches[0].clientX;
	const move = (x - startPosition) * 2;
	slider.scrollLeft = scroll - move;
}, false);

slider.addEventListener('touchleave', function(e) {
	isMouseDown = false;
}, false);

})();
