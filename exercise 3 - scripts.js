(function(){

if(!document.createElement("canvas").getContext) return;

var canvas = document.querySelector(".canvas"),
	ctx = canvas.getContext("2d"),
	y = 0,
	counter = 1,
	timeout,
	startPos,
	check = 0, 
	canvasWidth = 800,
	canvasHeight = 500;

function clear() {
	ctx.fillStyle = "#fff";
	ctx.fillRect(0, 0, canvasWidth, canvasHeight);
	ctx.fillStyle = "#000";
}

function draw() {
	ctx.fillStyle = "#000";

	for(var i = 1; i < 18; i++) {
		ctx.fillRect(0, y, canvasWidth, 20);

		y = y + 21 + i;

	}

	return y = 0;
}

draw();

canvas.addEventListener("mouseenter", function(e) {

	startPos = e.y;
	console.log(check);

}, false);


canvas.addEventListener("mousemove", function(e) {

	if(e.y - startPos < 0) {

		setTimeout(function() {
			if(check >= -canvasHeight) {
				clear();
				y = 0;
				ctx.translate(0, -10);
				check -= 10;
				console.log(check);
				draw();
				startPos = e.y;
			}

		}, 10);

	} else if(e.y - startPos > 0) {

		setTimeout(function() {
			if(check < 0) {
				clear();
				y = 0;
				ctx.translate(0, 10);
				check += 10;
				console.log(check);
				draw();
				startPos = e.y;
			}

		}, 20);

	}

}, false);

})();